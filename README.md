### Prerequisites ###

Please install [grunt](https://gruntjs.com/) and [bower](https://bower.io/)

### Change settings ###

Change settings in file `app/js/settings.js`

### Download dependencies and build ###
	
	npm install