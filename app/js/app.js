"use strict";

var appContacts = angular.module("app-contacts", [
    // Third party modules
    "ui.router",
    "ngAnimate",
    // Modules
    "contactsModule"
]);

appContacts.controller("appController", function($scope) {
    $scope.userMenuVisible = false;

    $scope.toggleUserMenu = function($event) {
        $event.stopPropagation();
        $scope.userMenuVisible = !$scope.userMenuVisible;
    };

    $scope.hideUserMenu = function() {
        $scope.userMenuVisible = false;
    };
});

appContacts.config([
    "$stateProvider",
    "$urlRouterProvider",
    function($stateProvider, $urlRouterProvider) {
        // Redirect to index state from any invalid state
        $urlRouterProvider.otherwise("/");
    }
]);
