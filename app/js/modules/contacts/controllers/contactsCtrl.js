contactsModule.controller("contactsCtrl", [
    "$scope",
    "$state",
    "settings",
    "$timeout",
    "$window",
    "contactsService",
    function($scope, $state, settings, $timeout, $window, contactsService) {
        $scope.contacts = [];
        $scope.contactsFiltered = [];
        $scope.cities = [];
        $scope.selectedContact = null;
        $scope.loading = true;

        $scope.filter = {
            name: "",
            city: "",
            active: true
        };

        $scope.sort = {
            field: "name",
            asc: true
        };

        $scope.resetSort = function() {
            $scope.sort = {
                field: "name",
                asc: true
            };
        };

        $scope.filterContacts = function() {
            $scope.resetSort();
            $scope.contactsFiltered = contactsService.filterList(
                $scope.contacts,
                $scope.filter,
                $scope.sort
            );

            // $scope.selectedContact = null;
            $scope.selectFirstContact();
        };

        $scope.sortList = function(field) {
            if ($scope.sort.field == field) {
                $scope.sort.asc = !$scope.sort.asc;
            } else {
                $scope.sort.field = field;
                $scope.sort.asc = true;
            }

            $scope.contactsFiltered = contactsService.sortList(
                $scope.contactsFiltered,
                $scope.sort
            );
        };

        $scope.getData = function() {
            contactsService.getList().then(function(response) {
                $scope.contacts = response.data;
                $scope.cities = contactsService.getCitiesList(response.data);

                $scope.contactsFiltered = contactsService.sortList(
                    $scope.contacts.slice(),
                    $scope.sort
                );
                
                $scope.selectFirstContact();

                // simulate longer loading
                $timeout(function() {
                    $scope.loading = false;
                }, 500);
            });
        };

        $scope.selectFirstContact = function() {
            if ($scope.contactsFiltered.length) {
                $scope.selectedContact = $scope.contactsFiltered[0];
            }
        };

        $scope.showContactDetails = function(id) {
            $scope.selectedContact = contactsService.getContactByID($scope.contacts, id);
        };

        $scope.editContact = function(id, $event) {
            $event.stopPropagation();
            console.log("edit contact", id);
        };

        $scope.deleteContact = function(id, $event) {
            $event.stopPropagation();
            console.log("delete contact", id);
        };

        $scope.getData();
    }
]);
