var contactsModule = angular.module("contactsModule", []);

contactsModule.constant("contactsSettings", {
    rootPath: "js/modules/contacts"
});

contactsModule.config([
    "$stateProvider",
    "$urlRouterProvider",
    "contactsSettings",
    function($stateProvider, $urlRouterProvider, contactsSettings) {
        $stateProvider.state("index", {
            url: "/",
            views: {
                content: {
                    templateUrl: contactsSettings.rootPath + "/views/main.html",
                    controller: "contactsCtrl"
                }
            }
        });
    }
]);
