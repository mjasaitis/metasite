contactsModule.factory("contactsService", [
	"$http",
	"settings",
	function($http, settings) {
		"use strict";

		var dataUrl = settings.rootUrl + settings.dataPath;

		return {
			getList: function() {
				return $http.get(dataUrl);
			},

			delete: function(id) {
				// call API method to delete contact
			},

			getCitiesList: function(contacts) {
				var cities = [];
				angular.forEach(contacts, function(contact) {
					if (cities.indexOf(contact.city) == -1) {
						cities.push(contact.city);
					}
				});

				return cities.sort();
			},

			filterList: function(list, filter, sort) {
				if (filter.name != "") {
					list = list.filter(function(item) {
						return (
							item.name.toLowerCase().indexOf(filter.name.toLowerCase()) != -1 ||
							item.surname.indexOf(filter.name.toLowerCase()) != -1
						);
					});
				}

				if (filter.city != "") {
					list = list.filter(function(item) {
						return item.city === filter.city;
					});
				}

				if (filter.active) {
					list = list.filter(function(item) {
						return item.active === true;
					});
				}

				if (typeof sort == "object") {
					list = this.sortList(list, sort);
				}

				return list;
			},

			sortList: function(list, sort) {
				if (sort.field == "name") {
					if (sort.asc === true) {
						list = list.sort(function(a, b) {
							return a.name > b.name ? 1 : b.name > a.name ? -1 : 0;
						});
					} else {
						list = list.sort(function(a, b) {
							return b.name > a.name ? 1 : a.name > b.name ? -1 : 0;
						});
					}
				}

				return list;
			},

			getContactByID: function(contacts, id) {
				var index = contacts
					.map(function(e) {
						return e.id;
					})
					.indexOf(id);
				if (index == -1) {
					return null;
				} else {
					return contacts[index];
				}
			}
		};
	}
]);
