module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),

        scssPath: "./scss/",
        cssPath: "./app/css/",

        cssmin: {
            expand: false,
            css: {
                src: "<%= cssPath %>/main.css",
                dest: "<%= cssPath %>/main.min.css"
            }
        },

        sass: {
            options: {
                require: "susy",
                sourceMap: true
            },
            css: {
                options: {
                    includePaths: ["node_modules/susy/sass"],
                    require: "susy",
                    sourceMap: true
                },

                files: {
                    "<%= cssPath %>/main.css": "<%= scssPath %>/main.scss"
                }
            }
        },

        watch: {
            css: {
                files: ["<%= scssPath %>/**/*.scss"],
                tasks: ["sass:css", "cssmin:css"]
            }
        },

        watch: {
            css: {
                files: ["<%= scssPath %>/**/*.scss"],
                tasks: ["sass:css", "cssmin:css"]
            }
        }


    });

    grunt.loadNpmTasks("grunt-sass");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-contrib-cssmin");

    grunt.registerTask("default", ["watch"]);
    grunt.registerTask("build", ["sass:css", "cssmin:css"]);
};
